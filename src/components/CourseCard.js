import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';

// 

export default function CourseCard({courseProp}) {

	//console.log(courseProp);

	const {name, description, price} = courseProp;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
	// console.log(useState(0));

	// Function that keeps track of the enrollees for a course
	// By default JavaScript is synchronous, as it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next.
	// The setter function for useStates are asynchronous, allowing it to execute separately from other codes in the program.
	// The "setCount" function is being executed while the "console.log" is already being completed resulting in the console to be behind by one count.

	function enroll () {
		if ((count + 1) <= 30 && (seats - 1) >= 0) {
			setCount(count + 1);
			setSeats(seats - 1);
			console.log('Enrollees: ' + count);
			console.log('Seats: ' + seats);
		} else {
			alert('No more seats available.')
		}
	}

	return(
       	<Card className="mb-2">
       	    <Card.Body>
       	        <Card.Title>{name}</Card.Title>
       	        <Card.Subtitle>Description:</Card.Subtitle>
       	        <Card.Text>{description}</Card.Text>
       	        <Card.Subtitle>Price:</Card.Subtitle>
       	        <Card.Text>PhP {price}</Card.Text>
       	        <Card.Text>Enrollees: {count}</Card.Text>
       	        <Card.Text>Seats: {seats}</Card.Text>
       	        <Button size="lg" variant="primary" onClick={enroll}>Enroll</Button>
       	    </Card.Body>
       	</Card>
	)
}